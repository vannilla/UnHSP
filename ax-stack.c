/* This file is part of unax. */

/* unax is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* unax is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with unax.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdlib.h>
#include <stdint.h>
#include <assert.h>
#include "ax.h"
#include "ax-stack.h"

/* Hopefully no one writes code so bad it needs more than 1024
   elements on the stack... */
#define MAX_STACK 1024

static struct ax_stack stack[MAX_STACK];

/* -1 means the stack is empty */
static int top = -1;

void ax_stack_push(enum ax_types type, uint32_t code,
		   char const *word, uint16_t flags) {
     /* We assert here because if the stack is full it's a better
	idea to reconsider the analyzed code than trying to recover */
     assert(top < MAX_STACK);

     if (top == -1) {
	  top = 0;
     }
     
     stack[top].type = type;
     stack[top].code = code;
     stack[top].word = (char *)word;
     stack[top].flags = flags;

     ++top;
}

int ax_stack_pop(enum ax_types *type, uint32_t *code,
		 char **word, uint16_t *flags) {
     int i;
     
     if (type == NULL || code == NULL || word == NULL || flags == NULL) {
	  /* Return nothing if any of these are NULL because
	     it doesn't make sense to get only partial informations */
	  return -1;
     }

     if (top == -1) {
	  return -1;
     }

     i = top-1;
     if (i < 0) {
	  return -1;
     }

     *type = stack[i].type;
     *code = stack[i].code;
     *word = stack[i].word;
     *flags = stack[i].flags;

     --top;

     return 0;
}
