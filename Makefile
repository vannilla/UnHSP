CFLAGS=-Wall -pedantic -std=gnu11 -Wextra

all: unexe undpm uncrypt unax

unexe: unexe.o

undpm: undpm.o

uncrypt: uncrypt.o

unax: unax.o sjis.o ax.o ax-stack.o

unexe.o: unexe.c

undpm.o: undpm.c

uncrypt.o: uncrypt.c

unax.o: unax.c

sjis.o : sjis.c

ax.o: ax.c

ax-stack.o: ax-stack.c

.PHONY: clean

clean:
	rm *.o
