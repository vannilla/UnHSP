/* This file is part of unax. */

/* unax is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* unax is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with unax.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdint.h>
#include <stdlib.h>

#include "sjis-table.h"
#include "sjis.h"

static int sjis_compare(const void *a, const void *b) {
     const struct sjis_table *s1 = (const struct sjis_table *)a;
     const struct sjis_table *s2 = (const struct sjis_table *)b;

     return (s1->index > s2->index) - (s1->index < s2->index);
}

void sjis_init(void) {
     qsort(sjis_table, (&sjis_table)[1] - sjis_table/* 7924 */,
	   sizeof(struct sjis_table), sjis_compare);
}

int sjis_is_ascii(uint8_t index) {
     return !(index >= 0x81 && index <= 0x9F)
	  && !(index >= 0xE0 && index <= 0xFC);
}

char *sjis_char(uint16_t index) {
     struct sjis_table target;
     struct sjis_table *res;

     target.index = index;
     
     res = (struct sjis_table *)bsearch(&target, sjis_table,
					(&sjis_table)[1] - sjis_table/* 7924 */,
					sizeof(struct sjis_table),
					sjis_compare);
     if (res == NULL) {
	  return "�";
     } else {
	  return res->value;
     }
}
