/* This program is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* This program is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <inttypes.h>
#include <time.h>
#include <argp.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <error.h>

#define READ(place, size) read(STDIN_FILENO, &(place), size)
#define READBUF(place, size) read(STDIN_FILENO, place, size); cur_pos += size
#define WRITE(place, size) write(STDOUT_FILENO, &(place), size)
#define WRITEBUF(place, size) write(STDOUT_FILENO, place, size)

#define checked_read(place, size)			\
     do {						\
	  if (READ(place, size) <= 0) {			\
	       error(1, errno, "Error while reading");	\
	  }						\
	  cur_pos += size;				\
     } while (0)

/* Typedefs to more easily align with Microsoft's specifications */
typedef uint8_t BYTE;
typedef uint16_t WORD;
typedef uint32_t DWORD;
typedef uint64_t QWORD;

struct dos_header {
     WORD magic;
     WORD last_bytes;
     WORD page_number;
     WORD reloc_number;
     WORD header_size;
     WORD header_min;
     WORD header_max;
     WORD ss;
     WORD sp;
     WORD checksum;
     WORD ip;
     WORD cs;
     WORD reloc_first;
     WORD overlay;
     WORD reserved[4];
     WORD oem_id;
     WORD oem_info;
     WORD reserved2[10];
     DWORD offset;
};

struct dos_section {
     WORD offset;
     WORD segment;
};

struct nt_header {
     DWORD magic;
     WORD machine;
     WORD section_num;
     DWORD timestamp;
     DWORD symtable;
     DWORD symtable_num;
     WORD optional_size;
     WORD flags;
};

struct nt_optional {
     WORD magic;
     BYTE major;
     BYTE minor;
     DWORD section_size;
     DWORD init_size;
     DWORD uninit_size;
     DWORD entrypoint;
     DWORD code_base;
     union {
	  struct {
	       DWORD data_base;
	       DWORD image_base;
	  } pe32;
	  QWORD pe64;
     } image_or_data_base;
     DWORD align;
     DWORD file_align;
     WORD os_major;
     WORD os_minor;
     WORD image_major;
     WORD image_minor;
     WORD sys_major;
     WORD sys_minor;
     DWORD win32ver;
     DWORD image_size;
     DWORD head_size;
     DWORD checksum;
     WORD subsys;
     WORD dll_flags;
     union {
	  DWORD pe32;
	  QWORD pe64;
     } stack_reserve;
     union {
	  DWORD pe32;
	  QWORD pe64;
     } stack_commit;
     union {
	  DWORD pe32;
	  QWORD pe64;
     } heap_reserve;
     union {
	  DWORD pe32;
	  QWORD pe64;
     } heap_commit;
     DWORD loader_flags;
     DWORD rva_size;
};

struct nt_dir {
     DWORD table;
     DWORD size;
};

struct nt_section {
     QWORD name;
     DWORD vsize;
     DWORD vaddress;
     DWORD raw_size;
     DWORD raw_pointer;
     DWORD reloc_pointer;
     DWORD linum_pointer;
     WORD reloc_num;
     WORD linum_num;
     DWORD flags;
};

error_t argp_parser(int key, char *arg, struct argp_state *state);
void print_dos(struct dos_header dos);
void print_nt(struct nt_header nt);
void print_nt_optional(struct nt_optional nt);

const char *argp_program_version = "1.0";
const char *argp_program_bug_address = "<vannilla@firemail.cc>";

struct argp_option argp_options[] = {
     {"info", 'i', NULL, 0, "Print informations about the EXE file "
      "to standard error.", 0},
     {"dos", 'D', NULL, 0, "Read only the DOS header and write the rest "
      "of the file to standard output.", 0},
     {0}
};

struct argp argp_data = {
     argp_options, argp_parser, NULL,
     "Read an EXE file from standard input, remove the EXE headers, "
     "then print the result to standard output.",
     NULL, NULL, NULL
};

int info = 0;
int dos_only = 0;

BYTE buffer[512];

size_t cur_pos = 0;

int main(int argc, char *argv[]) {
     struct dos_header dos;
     struct dos_section *ndos;
     struct nt_header nt;
     struct nt_optional nopt;
     struct nt_dir *ndirs;
     struct nt_section *nsec, nlast;

     ssize_t rr;
     size_t i;
     
     WORD ntstart;
     WORD w;
     BYTE tmp;

     argp_parse(&argp_data, argc, argv, 0, NULL, NULL);

     checked_read(dos.magic, sizeof(WORD));
     
     if (dos.magic != 0x5A4D) {
	  error(1, 0, "Invalid magic number in DOS stub");
     }

     /* Read DOS header */
     checked_read(dos.last_bytes, sizeof(WORD));
     checked_read(dos.page_number, sizeof(WORD));
     checked_read(dos.reloc_number, sizeof(WORD));
     checked_read(dos.header_size, sizeof(WORD));
     checked_read(dos.header_min, sizeof(WORD));
     checked_read(dos.header_max, sizeof(WORD));
     checked_read(dos.ss, sizeof(WORD));
     checked_read(dos.sp, sizeof(WORD));
     checked_read(dos.checksum, sizeof(WORD));
     checked_read(dos.ip, sizeof(WORD));
     checked_read(dos.cs, sizeof(WORD));
     checked_read(dos.reloc_first, sizeof(WORD));
     checked_read(dos.overlay, sizeof(WORD));

     for (i=0; i<4; ++i) {
	  checked_read(dos.reserved[i], sizeof(WORD));
     }

     checked_read(dos.oem_id, sizeof(WORD));
     checked_read(dos.oem_info, sizeof(WORD));

     for (i=0; i<10; ++i) {
	  checked_read(dos.reserved2[i], sizeof(WORD));
     }

     checked_read(dos.offset, sizeof(DWORD));
     
     if (info != 0) {
	  print_dos(dos);
     }

     if (dos_only != 0) {
	  do {
	       memset(buffer, 0, 512*sizeof(BYTE));
	       rr = READBUF(buffer, 512*sizeof(BYTE));
	       
	       if (rr < 0) {
		    error(1, errno, "Error while reading");
	       }
	       
	       WRITEBUF(buffer, rr);
	  } while (rr > 0);
	  
	  exit(0);
     }

     /* Skip DOS relocation sections */
     if (dos.reloc_number > 0) {
	  ndos = (struct dos_section *)calloc(dos.reloc_number,
					      sizeof(struct dos_section));
	  if (ndos == NULL) {
	       error(1, ENOMEM, "Error while reading DOS header");
	  }
	  
	  for (i=0; i<dos.reloc_number; ++i) {
	       checked_read((ndos[i]).offset, sizeof(WORD));
	       checked_read((ndos[i]).segment, sizeof(WORD));
	  }

	  /* We don't actually need these */
	  free(ndos);
     }

     /* Skip DOS stub */
     ntstart = dos.offset - dos.header_size*16;
     for (w=0; w<ntstart; w+=sizeof(BYTE)) {
	  checked_read(tmp, sizeof(BYTE));
     }

     checked_read(nt.magic, sizeof(DWORD));

     if (nt.magic != 0x4550) {
	  error(1, 0, "Invalid magic number in PE header");
     }

     /* Read NT header */
     checked_read(nt.machine, sizeof(WORD));
     checked_read(nt.section_num, sizeof(WORD));
     checked_read(nt.timestamp, sizeof(DWORD));
     checked_read(nt.symtable, sizeof(DWORD));
     checked_read(nt.symtable_num, sizeof(DWORD));
     checked_read(nt.optional_size, sizeof(WORD));
     checked_read(nt.flags, sizeof(WORD));

     if (info != 0) {
	  print_nt(nt);
     }

     /* If no optional header, be done */
     if (nt.optional_size == 0) {
	  do {
	       memset(buffer, 0, 512*sizeof(BYTE));
	       rr = READBUF(buffer, 512*sizeof(BYTE));
	       
	       if (rr < 0) {
		    error(1, errno, "Error while reading");
	       }
	       
	       WRITEBUF(buffer, rr);
	  } while(rr > 0);

	  exit(0);
     }

     checked_read(nopt.magic, sizeof(WORD));

     if (nopt.magic == 0x107) {
	  error(1, 0, "Not a valid executable");
     }

     /* Read NT optional header */
     checked_read(nopt.major, sizeof(BYTE));
     checked_read(nopt.minor, sizeof(BYTE));
     checked_read(nopt.section_size, sizeof(DWORD));
     checked_read(nopt.init_size, sizeof(DWORD));
     checked_read(nopt.uninit_size, sizeof(DWORD));
     checked_read(nopt.entrypoint, sizeof(DWORD));
     checked_read(nopt.code_base, sizeof(DWORD));

     if (nopt.magic == 0x10b) {
	  checked_read(nopt.image_or_data_base.pe32.data_base, sizeof(DWORD));
	  checked_read(nopt.image_or_data_base.pe32.image_base, sizeof(DWORD));
     } else {
	  checked_read(nopt.image_or_data_base.pe64, sizeof(QWORD));
     }

     checked_read(nopt.align, sizeof(DWORD));
     checked_read(nopt.file_align, sizeof(DWORD));
     checked_read(nopt.os_major, sizeof(WORD));
     checked_read(nopt.os_minor, sizeof(WORD));
     checked_read(nopt.image_major, sizeof(WORD));
     checked_read(nopt.image_minor, sizeof(WORD));
     checked_read(nopt.sys_major, sizeof(WORD));
     checked_read(nopt.sys_minor, sizeof(WORD));
     checked_read(nopt.win32ver, sizeof(DWORD));
     checked_read(nopt.image_size, sizeof(DWORD));
     checked_read(nopt.head_size, sizeof(DWORD));
     checked_read(nopt.checksum, sizeof(DWORD));
     checked_read(nopt.subsys, sizeof(WORD));
     checked_read(nopt.dll_flags, sizeof(WORD));

     if (nopt.magic == 0x10b) {
	  checked_read(nopt.stack_reserve.pe32, sizeof(DWORD));
	  checked_read(nopt.stack_commit.pe32, sizeof(DWORD));
	  checked_read(nopt.heap_reserve.pe32, sizeof(DWORD));
	  checked_read(nopt.heap_commit.pe32, sizeof(DWORD));
     } else {
	  checked_read(nopt.stack_reserve.pe64, sizeof(QWORD));
	  checked_read(nopt.stack_commit.pe64, sizeof(QWORD));
	  checked_read(nopt.heap_reserve.pe64, sizeof(QWORD));
	  checked_read(nopt.heap_commit.pe64, sizeof(QWORD));
     }

     checked_read(nopt.loader_flags, sizeof(DWORD));
     checked_read(nopt.rva_size, sizeof(DWORD));

     if (info != 0) {
	  print_nt_optional(nopt);
     }

     /* Read RVA entries, if any */
     if (nopt.rva_size > 0) {
	  ndirs = (struct nt_dir *)calloc(nopt.rva_size, sizeof(struct nt_dir));

	  if (ndirs == NULL) {
	       error(1, ENOMEM, "Error while reading NT header");
	  }

	  for (i=0; i<nopt.rva_size; ++i) {
	       checked_read((ndirs[i]).table, sizeof(DWORD));
	       checked_read((ndirs[i]).size, sizeof(DWORD));
	  }

	  if ((ndirs[nopt.rva_size-1]).table != 0) {
	       error(1, 0, "Not end of table");
	  }

	  /* We don't actually need this */
	  free(ndirs);
     }

     /* Read NT sections, if any */
     if (nt.section_num > 0) {
	  nsec = (struct nt_section *)calloc(nt.section_num,
					     sizeof(struct nt_section));

	  if (nsec == NULL) {
	       error(1, ENOMEM, "Error while reading NT header");
	  }

	  for (i=0; i<nt.section_num; ++i) {
	       checked_read((nsec[i]).name, sizeof(QWORD));
	       checked_read((nsec[i]).vsize, sizeof(DWORD));
	       checked_read((nsec[i]).vaddress, sizeof(DWORD));
	       checked_read((nsec[i]).raw_size, sizeof(DWORD));
	       checked_read((nsec[i]).raw_pointer, sizeof(DWORD));
	       checked_read((nsec[i]).reloc_pointer, sizeof(DWORD));
	       checked_read((nsec[i]).linum_pointer, sizeof(DWORD));
	       checked_read((nsec[i]).reloc_num, sizeof(WORD));
	       checked_read((nsec[i]).linum_num, sizeof(WORD));
	       checked_read((nsec[i]).flags, sizeof(DWORD));
	  }
     }

     /* Keep reading until the raw data is reached */
     nlast = nsec[nt.section_num-1];
     
     while (cur_pos < nlast.raw_pointer+nlast.raw_size) {
	  READ(tmp, sizeof(BYTE));
	  cur_pos += sizeof(BYTE);
     }

     /* Finally, write the rest to stdout */
     do {
	  memset(buffer, 0, 512*sizeof(BYTE));
	  rr = READBUF(buffer, 512*sizeof(BYTE));
	       
	  if (rr < 0) {
	       error(1, errno, "Error while reading");
	  }
	       
	  WRITEBUF(buffer, rr);
     } while (rr > 0);
     
     return 0;
}

error_t argp_parser(int key, char *arg, struct argp_state *state) {
     /* Remove a warning */
     state = state;
     arg = arg;

     switch (key) {
     case 'i':
	  info = 1;
	  break;
     case 'D':
	  dos_only = 1;
	  break;
     default:
	  return ARGP_ERR_UNKNOWN;
     }
     
     return 0;
}

void print_dos(struct dos_header dos) {
     fprintf(stderr, "DOS header info:\n");
     fprintf(stderr,
	     "Bytes used in last block (0 means 512): %" PRIu16 "\n",
	     dos.last_bytes);
     fprintf(stderr,
	     "Pages used by the DOS program: %" PRIu16 "\n",
	     dos.page_number);
     fprintf(stderr,
	     "Number of relocation entries: %" PRIu16 "\n",
	     dos.reloc_number);
     fprintf(stderr,
	     "Size of the whole header in bytes: %" PRIu16 "\n",
	     dos.header_size*16);
     fprintf(stderr,
	     "Additional memory required by program: %" PRIu16 "\n",
	     dos.header_min);
     fprintf(stderr,
	     "Additional memory reserved to the program: %" PRIu16 "\n",
	     dos.header_max);
     fprintf(stderr,
	     "Stack segment offset: %" PRIu16 "\n",
	     dos.ss);
     fprintf(stderr,
	     "Stack pointer value: %" PRIu16 "\n",
	     dos.sp);
     fprintf(stderr,
	     "Checksum: %" PRIu16 "\n",
	     dos.checksum);
     fprintf(stderr,
	     "IP register value: %" PRIu16 "\n",
	     dos.ip);
     fprintf(stderr,
	     "CS register value: %" PRIu16 "\n",
	     dos.cs);
     fprintf(stderr,
	     "Relocation table offset: %" PRIu16 "\n",
	     dos.reloc_first);
     fprintf(stderr,
	     "Overlay: %" PRIu16 "\n",
	     dos.overlay);
     fprintf(stderr,
	     "Offset to PE executable: %" PRIu32 "\n\n",
	     dos.offset);
}

void print_nt(struct nt_header nt) {
     fprintf(stderr, "COFF file header info:\n");
     fprintf(stderr,
	     "Target machine (hexadecimal code): 0x%" PRIx16 "\n",
	     nt.machine);
     fprintf(stderr,
	     "Number of sections: %" PRIu16 "\n",
	     nt.section_num);
     fprintf(stderr,
	     "Timestamp: %s",
	     ctime((time_t *)&(nt.timestamp)));
     fprintf(stderr,
	     "Symbol table offset: %" PRIu32 "\n",
	     nt.symtable);
     fprintf(stderr,
	     "Number of symbols: %" PRIu32 "\n",
	     nt.symtable_num);
     fprintf(stderr,
	     "Size of optional header: %" PRIu16 "\n",
	     nt.optional_size);
     fprintf(stderr,
	     "Flags (as hexadecimal number): 0x%" PRIx16 "\n\n",
	     nt.flags);
}

void print_nt_optional(struct nt_optional nt) {
     fprintf(stderr, "Optional COFF header info:\n");
     fprintf(stderr,
	     "PE type (hexadecimal flag): 0x%" PRIx16 "\n",
	     nt.magic);
     fprintf(stderr,
	     "Linker major version: %" PRIu8 "\n",
	     nt.major);
     fprintf(stderr,
	     "Linker minor version: %" PRIu8 "\n",
	     nt.minor);
     fprintf(stderr,
	     "Size of code section: %" PRIu32 "\n",
	     nt.section_size);
     fprintf(stderr,
	     "Size of initialized data: %" PRIu32 "\n",
	     nt.init_size);
     fprintf(stderr,
	     "Size of uninitialized data: %" PRIu32 "\n",
	     nt.uninit_size);
     fprintf(stderr,
	     "Entrypoint offset: %" PRIu32 "\n",
	     nt.entrypoint);
     fprintf(stderr,
	     "Base of code: %" PRIu32 "\n",
	     nt.code_base);

     if (nt.magic == 0x10b) {
	  fprintf(stderr,
		  "Base of data: %" PRIu32 "\n",
		  nt.image_or_data_base.pe32.data_base);
	  fprintf(stderr,
		  "Base of image in memory: %" PRIu32 "\n",
		  nt.image_or_data_base.pe32.image_base);
     } else {
	  fprintf(stderr,
		  "Base of image in memory: %" PRIu64 "\n",
		  nt.image_or_data_base.pe64);
     }

     fprintf(stderr,
	     "Section alignment: %" PRIu32 "\n",
	     nt.align);
     fprintf(stderr,
	     "File alignment: %" PRIu32 "\n",
	     nt.file_align);
     fprintf(stderr,
	     "OS major version: %" PRIu16 "\n",
	     nt.os_major);
     fprintf(stderr,
	     "OS minor version: %" PRIu16 "\n",
	     nt.os_minor);
     fprintf(stderr,
	     "Image major version: %" PRIu16 "\n",
	     nt.image_major);
     fprintf(stderr,
	     "Image minor version: %" PRIu16 "\n",
	     nt.image_minor);
     fprintf(stderr,
	     "Subsystem major version: %" PRIu16 "\n",
	     nt.sys_major);
     fprintf(stderr,
	     "Subsystem minor version: %" PRIu16 "\n",
	     nt.sys_minor);
     fprintf(stderr,
	     "Image size: %" PRIu32 "\n",
	     nt.image_size);
     fprintf(stderr,
	     "Header size: %" PRIu32 "\n",
	     nt.head_size);
     fprintf(stderr,
	     "Checksum: %" PRIu32 "\n",
	     nt.checksum);
     fprintf(stderr,
	     "Subsystem (hexadecimal code): 0x%" PRIx16 "\n",
	     nt.subsys);

     if (nt.magic == 0x10b) {
	  fprintf(stderr,
		  "Size of stack reserve: %" PRIu32 "\n",
		  nt.stack_reserve.pe32);
	  fprintf(stderr,
		  "Size of stack committed: %" PRIu32 "\n",
		  nt.stack_commit.pe32);
	  fprintf(stderr,
		  "Size of heap reserve: %" PRIu32 "\n",
		  nt.heap_reserve.pe32);
	  fprintf(stderr,
		  "Size of heap committed: %" PRIu32 "\n",
		  nt.heap_commit.pe32);
     } else {
	  fprintf(stderr,
		  "Size of stack reserve: %" PRIu64 "\n",
		  nt.stack_reserve.pe64);
	  fprintf(stderr,
		  "Size of stack committed: %" PRIu64 "\n",
		  nt.stack_commit.pe64);
	  fprintf(stderr,
		  "Size of heap reserve: %" PRIu64 "\n",
		  nt.heap_reserve.pe64);
	  fprintf(stderr,
		  "Size of heap committed: %" PRIu64 "\n",
		  nt.heap_commit.pe64);
     }
     
     fprintf(stderr,
	     "Number of RVA fields: %" PRIu32 "\n\n",
	     nt.rva_size);
}
