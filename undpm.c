/* This program is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* This program is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <inttypes.h>
#include <argp.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <error.h>

#define READ(place, size) read(STDIN_FILENO, &(place), size)
#define READBUF(place, size) read(STDIN_FILENO, place, size); cur_pos += size
#define WRITE(place, size) write(STDOUT_FILENO, &(place), size)
#define WRITEBUF(place, size) write(STDOUT_FILENO, place, size)

#define checked_read(place, size)			\
     do {						\
	  if (READ(place, size) <= 0) {			\
	       error(1, errno, "Error while reading");	\
	  }						\
	  cur_pos += size;				\
     } while (0)

struct dpm_header {
     uint32_t magic;
     uint32_t data_offset;
     uint32_t file_count;
     uint32_t file_offset;
};

struct dpm_file {
     char name[16];
     uint32_t reserved;
     uint32_t crypt_key;
     uint32_t offset;
     uint32_t size;
};

error_t argp_parser(int key, char *arg, struct argp_state *state);
void print_dpm(struct dpm_header dpm);
void print_file(struct dpm_file file);

const char *argp_program_version = "1.0";
const char *argp_program_bug_address = "<vannilla@firemail.cc>";

struct argp_option argp_options[] = {
     {"info", 'i', NULL, 0, "Prints informations about the DPM package "
      "to standard error.", 0},
     {"nodata", 'n', NULL, 0, "Don't print data to standard output.", 0},
     {0}
};

struct argp argp_data = {
     argp_options, argp_parser, NULL,
     "Read a DPMX file from standard input and extract the packaged "
     "start.ax file, which is written to standard output.",
     NULL, NULL, NULL
};

int info = 0;
int nodata = 0;
size_t cur_pos = 0;

int main(int argc, char *argv[]) {
     struct dpm_header dpm;
     struct dpm_file *files;
     uint32_t i, p;
     uint32_t start_offset;
     ssize_t rr;
     uint8_t tmp;
     int done;
     uint8_t buffer[512];
     
     argp_parse(&argp_data, argc, argv, 0, NULL, NULL);

     checked_read(dpm.magic, sizeof(uint32_t));

     if (dpm.magic != 0x584d5044) {
	  error(1, 0, "Not a DPM file");
     }

     /* Read DPM header */
     checked_read(dpm.data_offset, sizeof(uint32_t));
     checked_read(dpm.file_count, sizeof(uint32_t));
     checked_read(dpm.file_offset, sizeof(uint32_t));

     if (info != 0) {
	  print_dpm(dpm);
     }

     if (dpm.file_count == 0) {
	  error(1, 0, "Malformed package");
     }

     /* Read file headers */
     files = (struct dpm_file *)calloc(dpm.file_count,
				       sizeof(struct dpm_file));

     if (files == NULL) {
	  error(1, ENOMEM, "Error when reading files");
     }

     for (i=0; i<dpm.file_count; ++i) {
	  memset((files[i]).name, 0, 16*sizeof(char));
	  rr = READBUF((files[i]).name, 16*sizeof(char));
	       
	  if (rr < 0) {
	       error(1, 0, "Error while reading");
	  }

	  checked_read((files[i]).reserved, sizeof(uint32_t));
	  checked_read((files[i]).crypt_key, sizeof(uint32_t));
	  checked_read((files[i]).offset, sizeof(uint32_t));
	  checked_read((files[i]).size, sizeof(uint32_t));

	  if (info != 0) {
	       print_file(files[i]);
	  }
     }

     if (nodata != 0) {
	  exit(0);
     }

     /* Check that we are at the end of the header */
     while (cur_pos < dpm.data_offset) {
	  checked_read(tmp, sizeof(uint8_t));
     }

     /* Lookup start.ax */
     for (i=0, done=0; i<dpm.file_count && done==0; ++i) {
	  if (strncmp((files[i]).name, "start.ax", 16) == 0) {
	       done = 1;
	       p = i;
	       start_offset = (files[i]).offset;
	  }
     }

     /* Reach start.ax */
     while (cur_pos < start_offset+dpm.data_offset) {
	  checked_read(tmp, sizeof(uint8_t));
     }

     /* Write to standard output */
     cur_pos = 0;
     do {
	  memset(buffer, 0, 512*sizeof(uint8_t));
	  rr = READBUF(buffer, 512*sizeof(uint8_t));
	       
	  if (rr < 0) {
	       error(1, errno, "Error while reading");
	  }
	       
	  WRITEBUF(buffer, rr);
     } while (rr > 0 && cur_pos < (files[p]).size);
     
     return 0;
}

error_t argp_parser(int key, char *arg, struct argp_state *state) {
     /* Remove a warning */
     state = state;
     arg = arg;

     switch (key) {
     case 'i':
	  info = 1;
	  break;
     case 'n':
	  nodata = 1;
	  break;
     default:
	  return ARGP_ERR_UNKNOWN;
     }
     
     return 0;
}

void print_dpm(struct dpm_header dpm) {
     fprintf(stderr, "DPM header info:\n");
     fprintf(stderr, "Header size: %" PRIu32 "\n", dpm.data_offset);
     fprintf(stderr, "Number of files in package: %" PRIu32 "\n",
	     dpm.file_count);
     fprintf(stderr, "Offset to first file header: %" PRIu32 "\n\n",
	     dpm.file_offset);
}

void print_file(struct dpm_file file) {
     fprintf(stderr, "Packaged file informations:\n");
     fprintf(stderr, "Name: %16s\n", file.name);
     fprintf(stderr, "Encryption key (hexadecimal value): 0x%" PRIx32 "\n",
	     file.crypt_key);
     fprintf(stderr, "Offset to file data in bytes: %" PRIu32 "\n",
	     file.offset);
     fprintf(stderr, "File data size in bytes: %" PRIu32 "\n\n", file.size);
}
