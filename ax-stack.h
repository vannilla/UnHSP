#ifndef AX_STACK_H
#define AX_STACK_H

/* This file is part of unax. */

/* unax is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* unax is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with unax.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdint.h>
#include "ax.h"

struct ax_stack {
     enum ax_types type;
     uint32_t code;
     char *word;
     uint16_t flags;
};

void ax_stack_push(enum ax_types type, uint32_t code,
		   char const *word, uint16_t flags);
int ax_stack_pop(enum ax_types *type, uint32_t *code,
		 char **word, uint16_t *flags);

#endif
