/* This file is part of unax. */

/* unax is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* unax is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with unax.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include <inttypes.h>
#include <argp.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <error.h>

#include "sjis.h"
#include "ax.h"
#include "ax-stack.h"

#define READ(place, size) read(STDIN_FILENO, &(place), size)
#define READBUF(place, size) read(STDIN_FILENO, place, size); cur_pos += size
#define WRITE(place, size) write(STDOUT_FILENO, &(place), size)
#define WRITEBUF(place, size) write(STDOUT_FILENO, place, size)

#define checked_read(place, size)			\
     do {						\
	  if (READ(place, size) <= 0) {			\
	       error(1, errno, "Error while reading");	\
	  }						\
	  cur_pos += size;				\
     } while (0)

#define checked_alloc(place, type, size)				\
     do {								\
	  place = (type *)calloc(size, sizeof(type));			\
	  if (place == NULL) {						\
	       error(1, ENOMEM, "Can't allocate space for segment");	\
	  }								\
     } while (0)

#define with_current_position(current, global, body)	\
     do {						\
	  current = 0;					\
	  do {						\
	       body					\
	  } while (0);					\
	  global += current;				\
     } while (0)

struct ax_header {
     uint8_t magic[4];
     uint32_t version;
     uint32_t var_num;
     uint32_t total_size;
     uint32_t cs_offset;
     uint32_t cs_size;
     uint32_t ds_offset;
     uint32_t ds_size;
     uint32_t objtmp_offset;
     uint32_t objtmp_size;
     uint32_t debug_offset;
     uint32_t debug_size;
     uint32_t lib_offset;
     uint32_t lib_size;
     uint32_t fun_offset;
     uint32_t fun_size;
     uint32_t mod_offset;
     uint32_t mod_size;
     uint32_t fun2_offset;
     uint32_t fun2_size;
     uint32_t plug_offset;
     uint16_t plug_size;
     uint16_t plug_num;
     uint32_t boot;
     uint32_t runtime;
};

error_t argp_parser(int key, char *arg, struct argp_state *state);
void print_header(struct ax_header axh);
void hsp_emit_line(uint8_t *ds, uint8_t *ot);

const char *argp_program_version = "1.0";
const char *argp_program_bug_address = "<vannilla@firemail.cc>";

struct argp_option argp_options[] = {
     {"info", 'i', NULL, 0, "Print to standard error informations "
      "about the file.", 0},
     {"utf8", 'u', NULL, 0, "Decode characters with UTF-8", 0},
     {0}
};

struct argp argp_data = {
     argp_options, argp_parser, NULL,
     "Read an AX file from standard input and print to standard output the "
     "HSP source language code obtained from decompiling the AX bytecode.",
     NULL, NULL, NULL
};

size_t cur_pos = 0;
int info = 0;
int utf = 0;

int main(int argc, char *argv[]) {
     struct ax_header axh;
     uint8_t tmp;
     uint16_t val;
     uint16_t type;
     uint8_t len;
     uint32_t code;
     uint32_t i, q;
     char const *word;
     uint8_t *cs, *ds, *ot, *db, *libs, *funs, *mods, *fun2s, *pls;
     ssize_t rr;
     size_t pos;
     
     argp_parse(&argp_data, argc, argv, 0, NULL, NULL);

     sjis_init();
     ax_init();

     memset(axh.magic, 0, 4);
     READBUF(axh.magic, 4*sizeof(uint8_t));

     if (strncmp((char *)axh.magic, "HSP3", 4) != 0) {
	  error(1, 0, "Invalid magic");
     }

     /* Read the AX header */
     checked_read(axh.version, sizeof(uint32_t));
     checked_read(axh.var_num, sizeof(uint32_t));
     checked_read(axh.total_size, sizeof(uint32_t));
     checked_read(axh.cs_offset, sizeof(uint32_t));
     checked_read(axh.cs_size, sizeof(uint32_t));
     checked_read(axh.ds_offset, sizeof(uint32_t));
     checked_read(axh.ds_size, sizeof(uint32_t));
     checked_read(axh.objtmp_offset, sizeof(uint32_t));
     checked_read(axh.objtmp_size, sizeof(uint32_t));
     checked_read(axh.debug_offset, sizeof(uint32_t));
     checked_read(axh.debug_size, sizeof(uint32_t));
     checked_read(axh.lib_offset, sizeof(uint32_t));
     checked_read(axh.lib_size, sizeof(uint32_t));
     checked_read(axh.fun_offset, sizeof(uint32_t));
     checked_read(axh.fun_size, sizeof(uint32_t));
     checked_read(axh.mod_offset, sizeof(uint32_t));
     checked_read(axh.mod_size, sizeof(uint32_t));
     checked_read(axh.fun2_offset, sizeof(uint32_t));
     checked_read(axh.fun2_size, sizeof(uint32_t));
     checked_read(axh.plug_offset, sizeof(uint32_t));
     checked_read(axh.plug_size, sizeof(uint16_t));
     checked_read(axh.plug_num, sizeof(uint16_t));
     checked_read(axh.boot, sizeof(uint32_t));
     checked_read(axh.runtime, sizeof(uint32_t));

     if (info != 0) {
	  print_header(axh);
     }

     while (cur_pos < axh.cs_offset) {
	  checked_read(tmp, sizeof(uint8_t));
     }

     code = 0;

     /* The data segment and object temp contents need to be available
	while reading the code segment, so they must be read in their
	entirety before doing anything (thus the code segment too) */
     checked_alloc(cs, uint8_t, axh.cs_size);
     checked_alloc(ds, uint8_t, axh.ds_size);
     checked_alloc(ot, uint8_t, axh.objtmp_size);

     /* These segments are read for convenience */
     if (axh.debug_size > 0) {
	  checked_alloc(db, uint8_t, axh.debug_size);
     }
     if (axh.lib_size > 0) {
	  checked_alloc(libs, uint8_t, axh.lib_size);
     }
     if (axh.fun_size > 0) {
	  checked_alloc(funs, uint8_t, axh.fun_size);
     }
     if (axh.mod_size > 0) {
	  checked_alloc(mods, uint8_t, axh.mod_size);
     }
     if (axh.fun2_size > 0) {
	  checked_alloc(fun2s, uint8_t, axh.fun2_size);
     }
     if (axh.plug_size > 0) {
	  checked_alloc(pls, uint8_t, axh.plug_size);
     }

     pos = cur_pos;

     with_current_position(
	  cur_pos, pos,
	  while (cur_pos < axh.cs_size) {
	       rr = READBUF(cs+cur_pos, sizeof(uint16_t));
	       if (rr < 0) {
		    error(1, errno, "Error while reading");
	       }
	  });

     with_current_position(
	  cur_pos, pos,
	  while (cur_pos < axh.ds_size) {
	       rr = READBUF(ds+cur_pos, sizeof(uint16_t));
	       if (rr < 0) {
		    error(1, errno, "Error while reading");
	       }
	  });

     with_current_position(
	  cur_pos, pos,
	  while (cur_pos < axh.objtmp_size) {
	       rr = READBUF(ot+cur_pos, sizeof(uint16_t));
	       if (rr < 0) {
		    error(1, errno, "Error while reading");
	       }
	  });

     if (axh.debug_size > 0) {
	  with_current_position(
	       cur_pos, pos,
	       while (cur_pos < axh.debug_size) {
		    rr = READBUF(db+cur_pos, sizeof(uint16_t));
		    if (rr < 0) {
			 error(1, errno, "Error while reading");
		    }
	       });
     }

     if (axh.lib_size > 0) {
	  with_current_position(
	       cur_pos, pos,
	       while (cur_pos < axh.lib_size) {
		    rr = READBUF(libs+cur_pos, sizeof(uint16_t));
		    if (rr < 0) {
			 error(1, errno, "Error while reading");
		    }
	       });
     }

     if (axh.fun_size > 0) {
	  with_current_position(
	       cur_pos, pos,
	       while (cur_pos < axh.fun_size) {
		    rr = READBUF(funs+cur_pos, sizeof(uint16_t));
		    if (rr < 0) {
			 error(1, errno, "Error while reading");
		    }
	       });
     }
     if (axh.mod_size > 0) {
	  with_current_position(
	       cur_pos, pos,
	       while (cur_pos < axh.mod_size) {
		    rr = READBUF(mods+cur_pos, sizeof(uint16_t));
		    if (rr < 0) {
			 error(1, errno, "Error while reading");
		    }
	       });
     }
     if (axh.fun2_size > 0) {
	  with_current_position(
	       cur_pos, pos,
	       while (cur_pos < axh.fun2_size) {
		    rr = READBUF(fun2s+cur_pos, sizeof(uint16_t));
		    if (rr < 0) {
			 error(1, errno, "Error while reading");
		    }
	       });
     }
     if (axh.plug_size > 0) {
	  with_current_position(
	       cur_pos, pos,
	       while (cur_pos < axh.plug_size) {
		    rr = READBUF(pls+cur_pos, sizeof(uint16_t));
		    if (rr < 0) {
			 error(1, errno, "Error while reading");
		    }
	       });
     }

     if (axh.boot < axh.total_size && pos != axh.boot) {
	  error(1, 0, "Invalid offset");
     }

     i = 0; q = 0;
     while (i < axh.cs_size) {
	  val = ((uint16_t *)cs)[q]; i += sizeof(uint16_t); q += 1;
	  type = val & 0xFFF;
	  len = (val & 0x8000) & 0xFF;

	  code = (len != 0) ? ((uint32_t *)cs)[q] : ((uint16_t *)cs)[q];
	  i += (len != 0) ? sizeof(uint32_t) : sizeof(uint16_t);
	  q += (len != 0) ? 2 : 1;
	  word = ax_command(type, code);

	  if ((val & 0x2000) != 0) {
	       hsp_emit_line(ds, ot);
	       
	       /* Don't add the newline if it's the first command */
	       if (i != sizeof(uint32_t) && i != sizeof(uint16_t)) {
		    fprintf(stdout, "\n");
	       }
	       fprintf(stdout, "\t");
	  }

	  ax_stack_push((enum ax_types)type, code, word, val & 0x4000);
     }

     hsp_emit_line(ds, ot);
     fprintf(stdout, "\n");

     return 0;
}

error_t argp_parser(int key, char *arg, struct argp_state *state) {
     /* Remove a warning */
     state = state;
     arg = arg;

     switch (key) {
     case 'i':
	  info = 1;
	  break;
     case 'u':
	  utf = 1;
	  break;
     default:
	  return ARGP_ERR_UNKNOWN;
     }
     
     return 0;
}

void print_header(struct ax_header axh) {
     fprintf(stderr, "Header informations:\n");
     fprintf(stderr, "Version: 0x%" PRIx32 "\n", axh.version);
     fprintf(stderr, "Number of variables: %" PRIu32 "\n", axh.var_num);
     fprintf(stderr, "Total size (in bytes): %" PRIu32 "\n", axh.total_size);
     fprintf(stderr, "Offset to code segment: %" PRIu32 "\n", axh.cs_offset);
     fprintf(stderr, "Size of code segment: %" PRIu32 "\n", axh.cs_size);
     fprintf(stderr, "Offset to data segment: %" PRIu32 "\n", axh.ds_offset);
     fprintf(stderr, "Size of data segment: %" PRIu32 "\n", axh.ds_size);
     fprintf(stderr, "Offset to object temp: %" PRIu32 "\n", axh.objtmp_offset);
     fprintf(stderr, "Size of object temp: %" PRIu32 "\n", axh.objtmp_size);
     fprintf(stderr, "Offset to debug info: %" PRIu32 "\n", axh.debug_offset);
     fprintf(stderr, "Size of debug info: %" PRIu32 "\n", axh.debug_size);
     fprintf(stderr, "Offset to external libraries info: %" PRIu32 "\n",
	     axh.lib_offset);
     fprintf(stderr, "Size of external libraries info: %" PRIu32 "\n",
	     axh.lib_size);
     fprintf(stderr, "Offset to functions info: %" PRIu32 "\n", axh.fun_offset);
     fprintf(stderr, "Size of functions info: %" PRIu32 "\n", axh.fun_size);
     fprintf(stderr, "Offset to modules info: %" PRIu32 "\n", axh.mod_offset);
     fprintf(stderr, "Size of modules info: %" PRIu32 "\n", axh.mod_size);
     fprintf(stderr, "Offset to additional functions info: %" PRIu32 "\n",
	     axh.fun2_offset);
     fprintf(stderr, "Size of additional functions info: %" PRIu32 "\n",
	     axh.fun2_size);
     fprintf(stderr, "Offset to plugins info: %" PRIu32 "\n", axh.plug_offset);
     fprintf(stderr, "Size of plugins info: %" PRIu16 "\n", axh.plug_size);
     fprintf(stderr, "Number of plugins: %" PRIu16 "\n", axh.plug_num);
     fprintf(stderr, "Offset to boot info: %" PRIu32 "\n", axh.boot);
     fprintf(stderr, "Offset to runtime info: %" PRIu32 "\n", axh.runtime);
     fprintf(stderr, "\n");
}

void hsp_emit_line(uint8_t *ds, uint8_t *ot) {
     /* Duplicated code because things may change and
	making the ax_stack_* functions reentrant is
	wasteful */
     struct ax_stack stack[1024];
     size_t top = 0;
     uint32_t code;
     enum ax_types type;
     char *word;
     uint16_t flags;
     long i;

     uint32_t label;
     double dnum;

     while (ax_stack_pop(&type, &code, &word, &flags) == 0) {
	  stack[top].type = type;
	  stack[top].code = code;
	  stack[top].word = word;
	  stack[top].flags = flags;
	  ++top;
     }

     if (top == 0) {
	  /* Nothing was in the stack */
	  return;
     }

     for (i=top-1; i>-1; --i) {
	  if ((stack[i].flags & 0x4000) != 0) {
	       fprintf(stdout, ", ");
	  } else {
	       fprintf(stdout, " ");
	  }

	  switch (stack[i].type) {
	  case MARK:
	       if (stack[i].word != NULL) {
		    fprintf(stdout, "%s", stack[i].word);
	       } else {
		    if (utf != 0) {
			 fprintf(stdout, "%s",
				 sjis_char(stack[i].code & 0xFFFF));
		    } else {
			 fprintf(stdout, "%" PRIu32 "", stack[i].code);
		    }
	       }
	       break;
	  case VAR:
	       fprintf(stdout, "var%" PRIu32 "", stack[i].code);
	       break;
	  case STRING:
	       ax_emit_string(ds+stack[i].code, utf);
	       break;
	  case DNUM:
	       memcpy(&dnum, ds+stack[i].code, sizeof(double));
	       fprintf(stdout, "%g", dnum);
	       break;
	  case INUM:
	       fprintf(stdout, "%" PRIu32 "", stack[i].code);
	       break;
	  case STRUCT:
	       break;
	  case LABEL:
	       memcpy(&label, ot+(stack[i].code*sizeof(uint32_t)),
		      sizeof(uint32_t));
	       fprintf(stdout, "*label_%" PRIu32 "", label);
	       break;
	  case EXTCMD:
	  case PROGCMD:
	       if (stack[i].word != NULL) {
		    fprintf(stdout, "%s", stack[i].word);
	       }
	       break;
	  default:
	       fprintf(stdout, "{type: %" PRIu16 ", code: %" PRIu32 "}",
		       stack[i].type, stack[i].code);
	       break;
	  }
     }
}
