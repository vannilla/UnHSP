/* This program is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* This program is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <inttypes.h>
#include <argp.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <error.h>

#define READ(place, size) read(STDIN_FILENO, &(place), size)
#define READBUF(place, size) read(STDIN_FILENO, place, size); cur_pos += size
#define WRITE(place, size) write(STDOUT_FILENO, &(place), size)
#define WRITEBUF(place, size) write(STDOUT_FILENO, place, size)

#define checked_read(place, size)			\
     do {						\
	  if (READ(place, size) <= 0) {			\
	       error(1, errno, "Error while reading");	\
	  }						\
	  cur_pos += size;				\
     } while (0)

#define decrypt(byte, low, high) ((byte ^ low) - high)

error_t argp_parser(int key, char *arg, struct argp_state *state);
uint16_t make_dpm_key(uint32_t key);
uint16_t make_crypt_key(uint16_t key);

const char *argp_program_version = "1.0";
const char *argp_program_bug_address = "<vannilla@firemail.cc>";

struct argp_option argp_options[] = {
     {"nokey",  'n', NULL, 0, "Do not try decrypting the file.", 0},
     {0}
};

struct argp argp_data = {
     argp_options, argp_parser, NULL,
     "Read and decrypt a file extracted from a DPMX file from standard input. "
     "The decrypted data is then written to standard output. ",
     NULL, NULL, NULL
};

size_t cur_pos = 0;
int nokey = 0;

int main(int argc, char *argv[]) {
     uint8_t buffer[512];
     ssize_t rr;
     uint8_t magic[4], dmagic[4];
     int done;
     uint8_t low, high, tmp;
     uint16_t crypt;
     uint32_t i;
     
     argp_parse(&argp_data, argc, argv, 0, NULL, NULL);

     /* No decryption needed, write the data and exit */
     if (nokey != 0) {
	  do {
	       memset(buffer, 0, 512*sizeof(uint8_t));
	       rr = READBUF(buffer, 512*sizeof(uint8_t));
	       
	       if (rr < 0) {
		    error(1, errno, "Error while reading");
	       }
	       
	       WRITEBUF(buffer, rr);
	  } while (rr > 0);

	  exit(0);
     }

     checked_read(magic[0], sizeof(uint8_t));
     checked_read(magic[1], sizeof(uint8_t));
     checked_read(magic[2], sizeof(uint8_t));
     checked_read(magic[3], sizeof(uint8_t));

     memset(dmagic, 0, 4);
     done = 0;

     for (i=0; i<0xFFFFFFFF && done==0; ++i) {
	  crypt = make_crypt_key(make_dpm_key(i));

	  low = (crypt & 0x00FF) & 0xFF;
	  high = ((crypt & 0xFF00) >> 8) & 0xFF;

	  dmagic[0] = ((magic[0] ^ low) - high) & 0xFF;
	  dmagic[1] = (dmagic[0] + ((magic[1] ^ low) - high)) & 0xFF;
	  dmagic[2] = (dmagic[1] + ((magic[2] ^ low) - high)) & 0xFF;
	  dmagic[3] = (dmagic[2] + ((magic[3] ^ low) - high)) & 0xFF;

	  if (dmagic[0] == 'H' && dmagic[1] == 'S'
	      && dmagic[2] == 'P' && dmagic[3] == '3') {
	       done = 1;
	  }
     }

     if (done == 0) {
	  error(1, 0, "Can't find encryption key");
     }

     WRITEBUF(dmagic, 4*sizeof(uint8_t));

     i = dmagic[3];
     do {
	  rr = READ(tmp, sizeof(uint8_t));

	  if (rr < 0) {
	       error(1, errno, "Error while reading");
	  }

	  i += decrypt(tmp, low, high);

	  WRITE(i, sizeof(uint8_t));
     } while (rr > 0);

     return 0;
}

error_t argp_parser(int key, char *arg, struct argp_state *state) {
     /* Remove a warning */
     state = state;
     arg = arg;

     switch (key) {
     case 'n':
	  nokey = 1;
	  break;
     default:
	  return ARGP_ERR_UNKNOWN;
     }
     
     return 0;
}

uint16_t make_dpm_key(uint32_t key) {
     uint16_t loww, highw;
     uint8_t low1, low2;
     uint8_t high1, high2;
     uint8_t r1, r2;
     uint16_t ret;

     loww =  key & 0x0000FFFF;
     highw = (key & 0xFFFF0000) >> 16;

     low1 = (loww & 0x00FF) & 0xFF;
     low2 = (highw & 0x00FF) & 0xFF;
     high1 = ((loww & 0xFF00) >> 8) & 0xFF;
     high2 = ((highw & 0xFF00) >> 8) & 0xFF;

     r1 = ((low1 + 0x5A) ^ low2) & 0xFF;
     r2 = ((high1 + 0xA5) ^ high2) & 0xFF;

     ret = (r2 << 8) + r1;

     return ret;
}

uint16_t make_crypt_key(uint16_t key) {
     uint16_t exe;
     uint8_t low1, low2;
     uint8_t high1, high2;
     uint8_t r1, r2;
     uint16_t ret;

     exe = 0x55AA;

     low1 = (key & 0x00FF) & 0xFF;
     low2 = (exe & 0x00FF) & 0xFF;

     high1 = ((key & 0xFF00) >> 8) & 0xFF;
     high2 = ((exe & 0xFF00) >> 8) & 0xFF;

     r1 = (low1 + low2) & 0xFF;
     r2 = (high1 + high2) & 0xFF;

     ret = (r2 << 8) + r1;

     return ret;
}
