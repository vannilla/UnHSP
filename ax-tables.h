#ifndef AXTABLES_H
#define AXTABLES_H

/* This file is part of unax. */

/* unax is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* unax is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with unax.  If not, see <http://www.gnu.org/licenses/>. */

struct ax_type {
     uint32_t code;
     uint16_t type;
     char const *word;
};

struct ax_type operators[] = {
     {0x0, 0, "+"},
     {0x1, 0, "-"},
     {0x2, 0, "*"},
     {0x3, 0, "/"},
     {0x4, 0, "\\"},
     {0x5, 0, "&"},
     {0x6, 0, "|"},
     {0x7, 0, "^"},
     {0x8, 0, "="},
     {0x9, 0, "!="},
     {0xA, 0, ">"},
     {0xB, 0, "<"},
     {0xC, 0, ">="},
     {0xD, 0, "<="},
     {0xE, 0, ">>"},
     {0xF, 0, "<<"},
     {0x10, 0, ""},
     {0x28, 0, "("},
     {0x29, 0, ")"},
     {0x3F, 0, "?"},
};

struct ax_type cmpcmd[] = {
     {0x0, 11, "if"},
     {0x1, 11, "else"},
};

struct ax_type progcmd[] = {
     {0x0, 15, "goto"},
     {0x1, 15, "gosub"},
     {0x2, 15, "return"},
     {0x3, 15, "break"},
     {0x4, 15, "repeat"},
     {0x5, 15, "loop"},
     {0x6, 15, "continue"},
     {0x7, 15, "wait"},
     {0x8, 15, "await"},
     {0x9, 15, "dim"},
     {0xA, 15, "sdim"},
     {0xB, 15, "foreach"},
     {0xC, 15, "eachchk"},
     {0xD, 15, "dimtype"},
     {0xE, 15, "dup"},
     {0xF, 15, "dupptr"},
     {0x10, 15, "end"},
     {0x11, 15, "stop"},
     {0x12, 15, "newmod"},
     {0x13, 15, "setmod"},
     {0x14, 15, "delmod"},
     {0x15, 15, "alloc"},
     {0x16, 15, "mref"},
     {0x17, 15, "run"},
     {0x18, 15, "exgoto"},
     {0x19, 15, "on"},
     {0x1A, 15, "mcall"},
     {0x1B, 15, "assert"},
     {0x1C, 15, "logmes"},
     {0x1D, 15, "newlab"},
     {0x1E, 15, "resume"},
     {0x1F, 15, "yield"},
};

struct ax_type intcmd[] = {
     {0x0, 8, "onexit"},
     {0x1, 8, "onerror"},
     {0x2, 8, "onkey"},
     {0x3, 8, "onclick"},
     {0x4, 8, "oncmd"},
     {0x11, 8, "exist"},
     {0x12, 8, "delete"},
     {0x13, 8, "mkdir"},
     {0x14, 8, "chdir"},
     {0x15, 8, "dirlist"},
     {0x16, 8, "bload"},
     {0x17, 8, "bsave"},
     {0x18, 8, "bcopy"},
     {0x19, 8, "memfile"},
     {0x1A, 8, "poke"},
     {0x1B, 8, "wpoke"},
     {0x1C, 8, "lpoke"},
     {0x1D, 8, "getstr"},
     {0x1E, 8, "chdpm"},
     {0x1F, 8, "memexpand"},
     {0x20, 8, "memcpy"},
     {0x21, 8, "memset"},
     {0x22, 8, "notesel"},
     {0x23, 8, "noteadd"},
     {0x24, 8, "notedel"},
     {0x25, 8, "noteload"},
     {0x26, 8, "notesave"},
     {0x27, 8, "reandomize"},
     {0x28, 8, "noteunsel"},
     {0x29, 8, "noteget"},
     {0x2A, 8, "split"},
};

struct ax_type extcmd[] = {
     {0x0, 9, "button"},
     {0x1, 9, "chgdisp"},
     {0x2, 9, "exec"},
     {0x3, 9, "dialog"},
     {0x8, 9, "mmload"},
     {0x9, 9, "mmplay"},
     {0xA, 9, "mmstop"},
     {0xB, 9, "mci"},
     {0xC, 9, "pset"},
     {0xD, 9, "pget"},
     {0xE, 9, "syscolor"},
     {0xF, 9, "mes"},
     {0x10, 9, "title"},
     {0x11, 9, "pos"},
     {0x12, 9, "circle"},
     {0x13, 9, "cls"},
     {0x14, 9, "font"},
     {0x15, 9, "sysfont"},
     {0x16, 9, "objsize"},
     {0x17, 9, "picload"},
     {0x18, 9, "color"},
     {0x19, 9, "palcolor"},
     {0x1A, 9, "palette"},
     {0x1B, 9, "redraw"},
     {0x1C, 9, "width"},
     {0x1D, 9, "gsel"},
     {0x1E, 9, "gcopy"},
     {0x1F, 9, "gzoom"},
     {0x20, 9, "gmode"},
     {0x21, 9, "bmpsave"},
     {0x22, 9, "hsvcolor"},
     {0x23, 9, "getkey"},
     {0x24, 9, "listbox"},
     {0x25, 9, "chkbox"},
     {0x26, 9, "combox"},
     {0x27, 9, "input"},
     {0x28, 9, "mesbox"},
     {0x29, 9, "buffer"},
     {0x2A, 9, "screen"},
     {0x2B, 9, "bgscr"},
     {0x2C, 9, "mouse"},
     {0x2D, 9, "objsel"},
     {0x2E, 9, "groll"},
     {0x2F, 9, "line"},
     {0x30, 9, "clrobj"},
     {0x31, 9, "boxf"},
     {0x32, 9, "objprm"},
     {0x33, 9, "objmode"},
     {0x34, 9, "stick"},
     {0x35, 9, "grect"},
     {0x36, 9, "grotate"},
     {0x37, 9, "gsquare"},
     {0x38, 9, "gradf"},
     {0x38, 9, "objimage"},
     {0x39, 9, "objskip"},
     {0x3A, 9, "objenable"},
     {0x3B, 9, "celload"},
     {0x3C, 9, "celdiv"},
     {0x3D, 9, "celput"},
};

struct ax_type dllctrl[] = {
     {0x0, 17, "newcom"},
     {0x1, 17, "querycom"},
     {0x2, 17, "delcom"},
     {0x3, 17, "cnvstow"},
     {0x4, 17, "comres"},
     {0x5, 17, "axobj"},
     {0x6, 17, "winobj"},
     {0x7, 17, "sendmsg"},
     {0x8, 17, "comevent"},
     {0x9, 17, "comevarg"},
     {0xA, 17, "sarrayconv"},
     {0xB, 17, "variantref"},
     {0x100, 17, "callfunc"},
     {0x101, 17, "cnvwtos"},
     {0x102, 17, "comevdisp"},
     {0x103, 17, "libptr"},
};

struct ax_type sysvars[] = {
     {0x0, 14, "system"},
     {0x1, 14, "hspstat"},
     {0x2, 14, "hspver"},
     {0x3, 14, "stat"},
     {0x4, 14, "cnt"},
     {0x5, 14, "err"},
     {0x6, 14, "strsize"},
     {0x7, 14, "looplev"},
     {0x8, 14, "sublev"},
     {0x9, 14, "iparam"},
     {0xA, 14, "wparam"},
     {0xB, 14, "lparam"},
     {0xC, 14, "refstr"},
     {0xD, 14, "refdval"},
};

struct ax_type intfunc[] = {
     {0x0, 13, "int"},
     {0x1, 13, "rnd"},
     {0x2, 13, "strlen"},
     {0x3, 13, "length"},
     {0x4, 13, "length2"},
     {0x5, 13, "length3"},
     {0x6, 13, "length4"},
     {0x7, 13, "vartype"},
     {0x8, 13, "gettime"},
     {0x9, 13, "peek"},
     {0xA, 13, "wpeek"},
     {0xB, 13, "lpeek"},
     {0xC, 13, "varptr"},
     {0xD, 13, "varuse"},
     {0xE, 13, "noteinfo"},
     {0xF, 13, "instr"},
     {0x10, 13, "abs"},
     {0x11, 13, "limit"},
     {0x100, 13, "str"},
     {0x101, 13, "strmid"},
     {0x103, 13, "strf"},
     {0x104, 13, "getpath"},
     {0x105, 13, "strtrim"},
     {0x180, 13, "sin"},
     {0x181, 13, "cos"},
     {0x182, 13, "tan"},
     {0x183, 13, "atan"},
     {0x184, 13, "sqrt"},
     {0x185, 13, "double"},
     {0x186, 13, "absf"},
     {0x187, 13, "expf"},
     {0x188, 13, "logf"},
     {0x189, 13, "limitf"},
     {0x18A, 13, "powf"},
};

struct ax_type extvar[] = {
     {0x0, 10, "mousex"},
     {0x1, 10, "mousey"},
     {0x2, 10, "mousew"},
     {0x3, 10, "hwnd"},
     {0x4, 10, "hinstance"},
     {0x5, 10, "hdc"},
     {0x100, 10, "ginfo"},
     {0x101, 10, "objinfo"},
     {0x102, 10, "dirinfo"},
     {0x103, 10, "sysinfo"},
};

struct ax_type structype[] = {
     {0xFFFFFFFF, 5, "thismod"},
};

struct ax_type *type_tables[] = {
     operators, NULL, NULL, NULL, NULL, structype, NULL, NULL,
     intcmd, extcmd, extvar, cmpcmd, NULL, intfunc, sysvars,
     progcmd, NULL, dllctrl, NULL,
};

const size_t tables_length[] = {
     ((&operators)[1] - operators), 0, 0, 0, 0, ((&structype)[1] - structype),
     0, 0, ((&intcmd)[1] - intcmd), ((&extcmd)[1] - extcmd),
     ((&extvar)[1] - extvar), ((&cmpcmd)[1] - cmpcmd), 0,
     ((&intfunc)[1] - intfunc), ((&sysvars)[1] - sysvars),
     ((&progcmd)[1] - progcmd), 0, ((&dllctrl)[1] - dllctrl), 0,
};

#endif
