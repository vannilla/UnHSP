/* This file is part of unax. */

/* unax is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* unax is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with unax.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdint.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdio.h>

#include "ax-tables.h"
#include "ax.h"
#include "sjis.h"

static int ax_compare(const void *a, const void *b) {
     const struct ax_type *t1 = (const struct ax_type *)a;
     const struct ax_type *t2 = (const struct ax_type *)b;

     return (t1->code > t2->code) - (t1->code < t2->code);
}

void ax_init(void) {
     qsort(operators, tables_length[MARK], sizeof(struct ax_type), ax_compare);
     qsort(cmpcmd, tables_length[CMPCMD], sizeof(struct ax_type), ax_compare);
     qsort(progcmd, tables_length[PROGCMD], sizeof(struct ax_type), ax_compare);
     qsort(intcmd, tables_length[INTCMD], sizeof(struct ax_type), ax_compare);
     qsort(extcmd, tables_length[EXTCMD], sizeof(struct ax_type), ax_compare);
     qsort(dllctrl, tables_length[DLLCTRL], sizeof(struct ax_type), ax_compare);
     qsort(sysvars, tables_length[SYSVAR], sizeof(struct ax_type), ax_compare);
     qsort(intfunc, tables_length[INTFUNC], sizeof(struct ax_type), ax_compare);
     qsort(extvar, tables_length[EXTSYSVAR], sizeof(struct ax_type), ax_compare);
     qsort(structype, tables_length[STRUCT], sizeof(struct ax_type), ax_compare);
}

char const *ax_command(uint16_t type, uint32_t code) {
     struct ax_type *table;
     struct ax_type *res;

     enum ax_types axt = (enum ax_types)type;

     if (axt >= END) {
	  return NULL;
     }

     table = type_tables[axt];
     if (table == NULL) {
	  return NULL;
     }

     res = (struct ax_type *)bsearch(&code, table,
				     tables_length[axt], sizeof(struct ax_type),
				     ax_compare);

     if (res == NULL) {
	  return NULL;
     } else {
	  return res->word;
     }
}

void ax_emit_string(uint8_t *offset, int utf) {
     size_t j;
     char *word;
     uint16_t ch;
     
     if (utf != 0) {
	  fprintf(stdout, "\"%s\"", (char *)(offset));
     } else {
	  j = 0;
	  fprintf(stdout, "\"");
	  
	  while (*(offset+j) != '\0') {
	       if (sjis_is_ascii(*(offset+j)) == 0) {
		    ch = *(offset+j) << 8 | *(offset+j+1);
		    word = sjis_char(ch);
		    fprintf(stdout, "%s", word);
		    j += 2;
	       } else {
		    fprintf(stdout, "%c", *(offset+j));
		    j += 1;
	       }
	  }
	  
	  fprintf(stdout, "\"");
     }
}
