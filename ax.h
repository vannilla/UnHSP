#ifndef UNAX_H
#define UNAX_H

/* This file is part of unax. */

/* unax is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* unax is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with unax.  If not, see <http://www.gnu.org/licenses/>. */

enum ax_types {
     MARK = 0,
     VAR = 1,
     STRING = 2,
     DNUM = 3,
     INUM = 4,
     STRUCT = 5,
     XLABEL = 6,
     LABEL = 7,
     INTCMD = 8,
     EXTCMD = 9,
     EXTSYSVAR = 10,
     CMPCMD = 11,
     MODCMD = 12,
     INTFUNC = 13,
     SYSVAR = 14,
     PROGCMD = 15,
     DLLFUNC = 16,
     DLLCTRL = 17,
     USERDEF = 18,
     END, /* Sentinel */
};

void ax_init(void);
char const *ax_command(uint16_t type, uint32_t code);
void ax_emit_string(uint8_t *offset, int utf);

#endif
